#! /bin/bash

function comment() {
#yum install openstack-selinux
yum install mariadb mariadb-server python2-PyMySQL
cp openstack.cnf /etc/my.cnf.d
systemctl enable mariadb.service
systemctl start mariadb.service
mysql_secure_installation
yum install -y rabbitmq-server
systemctl enable rabbitmq-server.service
systemctl start rabbitmq-server.service
rabbitmqctl add_user openstack j@qkr2413 
rabbitmqctl set_permissions openstack ".*" ".*" ".*"
}

# yum install memcached python-memcached
cp memcached /etc/sysconfig
systemctl enable memcached.service
systemctl start memcached.service
